# ImagineCup
Imagine cup-Project submission

model to predict the pneumonia and other 14 types of diseases using Deeplearning models. 

The Model is trained on the following keras transfer learning libraries: <br>
VGG16<br>
VGG19<br>
DenseNet121<br>
ResNet50<br>
InceptionV3<br>
InceptionResNetV2<br>
NASNetMobile<br>
NASNetLarge<br>

## USE
Visit : 
https://checxray.herokuapp.com/

Upload any xray image you need to scan. [for now it supports only 1024*1024 image sizes]
There is a folder in the repos containing sample x-ray images of the required size.  
